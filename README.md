Here we setup and improve the basic structures used in Tomte.

These include: 
* a generic tree structure 
    + each node encapsulates a generic data type and maintains a list of referrences to children

* a generic graph structure
    + each node encapsulates a generic data type with set-able content
    + each edge implements a generic process,
    + the graph structure itself provides node/edge management, along
        with other features (connectivity, splitting) 
        

    