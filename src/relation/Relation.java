package relation;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

public enum Relation {
	SUCC, 
	EQUAL ; // the order is belangrijk
	
	private boolean active = false;
	
	public static Relation [] getActiveRelations() {
		Relation [] relations = Relation.values();
		List<Relation> activeRelations = new ArrayList<Relation>();
		for (Relation rel : relations) {
			if (rel.active) {
				activeRelations.add(rel);
			}
		}
		return activeRelations.toArray(new Relation[activeRelations.size()]);
	}
	
	public static void activateRelations(Collection<String> relations) {
		for (String relation : relations) {
			Relation.valueOf(relation.toUpperCase()).active = true;
		} 
	}  
	
	public static boolean hasRelatedValue (Collection <Integer> fromValues, Integer toValue) {
		boolean hasRelatedValue = false;
		for (Integer value : fromValues) {
			if (areRelated(value, toValue)) {
				hasRelatedValue = true;
				break;
			}
		}
		
		return hasRelatedValue;
	}
	
	/**
	 * Retrieves the values in {@code fromValues} which are related to values in {@code toValues} 
	 */
	/* TODO !! make method account for sequence of relations
	 * For from={0, 101, 102}, to={100} both 101 and 102 are related to 100 by a relation chain. 100 -> 101 -> 102 
	*/
	public static List<Integer> getRelatedValuesFrom (Collection <Integer> fromValues, Collection <Integer> toValues) {
		List<Integer> relatedValues = new ArrayList<Integer>();
		for (Integer toValue : toValues) {
			relatedValues.addAll(getRelatedValues(fromValues, toValue));
		}
		return relatedValues;
	}
	
	/**
	 * Retrieves the values in {@code toValues} which stem from values in {@code fromValues} 
	 */
	// TODO !! make method account for sequence of relations
	public static List<Integer> getRelatedValuesTo (Collection <Integer> fromValues, Collection <Integer> toValues) {
		List<Integer> relatedValues = new ArrayList<Integer>(); 
		for (Integer toValue : toValues) {
			if (Relation.hasRelatedValue(fromValues, toValue)) {
				relatedValues.add(toValue);
			}
		}
		return relatedValues;
	}
	
	public static List<Integer> getRelatedValues (Collection <Integer> fromValues, Integer toValue) {
		List<Integer> relatedValues = new ArrayList<Integer>(); 
		for (Integer value : fromValues) {
			if (areRelated(value, toValue)) {
				relatedValues.add(value);
			}
		}
		
		return relatedValues;
	}
	
	public static boolean areRelated(Integer val1, Integer val2) {
		boolean areRelated = false;
		for (Relation relation : Relation.values()) {
			if (relation.isSatisfied(val1, val2)) {
				areRelated = true;
				break;
			}
		}
		
		return areRelated;
	}
	
	public static LinkedHashSet<Relation> getRelations (Collection <Integer> values, Integer val) {
		LinkedHashSet<Relation> relations = new LinkedHashSet<Relation>();
		for (Integer value : values) {
			Relation relation = getRelation(value, val);
			if (relation != null) 
				relations.add(relation);
		}
		
		return relations;
	}
	
	public static Relation getRelation(Integer val1, Integer val2) {
		Relation ret = null;
		for (Relation relation : Relation.values()) {
			if (relation.isSatisfied(val1, val2)) {
				ret = relation;
				break;
			}
		}
		return ret;
	}
	
	public static Set<Integer> getRelatedValues(Integer val) {
		Set<Integer> relatedValues = new HashSet<Integer>();
		for (Relation relation : Relation.values()) {
			Integer relatedValue = relation.map(val);
			relatedValues.add(relatedValue);
			
		}
		return relatedValues;
	}
	
	public boolean isSatisfied(Integer val1, Integer val2) {
		if (!active) {
			return false;
		}
		switch(this.name()) {
		case "EQUAL": return val1.equals(val2);
		case "SUCC":
				return val2 == val1 - 1; // for fresh output values, the relation is reverse: -11 is succ for -10
		case "PRED":
				return val2 == val1 + 1; // for fresh output values, the relation is reverse: -11 is succ for -10
		}
		throw new BugException("This line should not have been reached");
	}
	
	public Integer map(Integer val) {
		if (!active) {
			throw new BugException("An inactive relation should never be mapped");
		}
		switch(this.name()) {
		case "EQUAL": return val;
		case "SUCC": 
				return val + 1;
		case "PRED":
				return val - 1; // for regular values, the relation is the standard: 11 is succ for 10
		}
		throw new BugException("This line should not have been reached");
	}
	
	public Integer rev(Integer val) {
		if (!active) {
			throw new BugException("An inactive relation should never be mapped");
		}
		switch(this.name()) {
		case "EQUAL": return val;
		case "SUCC": 
			return val - 1;
		}
		throw new BugException("This line should not have been reached");
	}
}
