package generictree;

import java.util.Stack;

public class GenericTreeIteratorUpwards<T> extends GenericTreeIteratorAbstract<T> implements GenericTreeIterator<GenericTreeNode<T>> {

    protected Stack<GenericTreeNode<T>> visiting = new Stack<GenericTreeNode<T>>(); 
    
  
    //protected int currentDepth;
    
    /** Construct an GenericTreeIteratorDepthFirstPreOrder object.
     * @param startNode The node of the tree where to start the iteration.
     */
    public GenericTreeIteratorUpwards(GenericTreeNode<T> startNode) {        
        super(startNode);
    	setStartNode(startNode);
    }
    
   // public GenericTreeIteratorDepthFirstPreOrder() {
   // }
    public GenericTreeIteratorUpwards<T> newInstance() {
    	return new GenericTreeIteratorUpwards<T>(this.startNode);
    }     
    
	/** Reset the iterator.
	 */
	public void reset() {
		this.nextNode=this.startNode;
	}
	

	
	/** Fetches the next node from the tree,
	 *  Note: this function is only called when next node exists!
	 */	
	public GenericTreeNode<T> fetchNext(){
		GenericTreeNode<T> node=this.nextNode;
    	this.nextNode=this.nextNode.getParent();       	      	
    	return node;   		
    }
	

}    
