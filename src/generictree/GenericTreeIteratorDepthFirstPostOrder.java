package generictree;

import java.util.Stack;
import java.util.LinkedList;


public class GenericTreeIteratorDepthFirstPostOrder<T> extends GenericTreeIteratorAbstract<T> implements GenericTreeIterator<GenericTreeNode<T>> {

    
    protected Stack<Integer> childIndexes = new Stack<Integer>(); 
    // childIndexes use stack interface (FIFO), choosen for fast push(end) and pop(end) FIFO style

    protected int currentNodeIndex;    
   
    protected LinkedList<GenericTreeNode<T>> nodeCache = new LinkedList<GenericTreeNode<T>>();  
    // nodeCache uses queue (LIFO) interface of linked list, choosen for fast add(end) and remove(begin) LIFO style )

    protected GenericTreeNode<T> currentNode; // position where you are in tree walk
    
    //protected int currentDepth;

    /** Construct an GenericTreeIteratorDepthFirstPostOrder object.
     * @param startNode The node of the tree where to start the iteration.
     */
    public GenericTreeIteratorDepthFirstPostOrder(GenericTreeNode<T> startNode) {
    	super(startNode);
    	setStartNode(startNode);
    }    

    public GenericTreeIteratorDepthFirstPostOrder<T> newInstance() {
    	return new GenericTreeIteratorDepthFirstPostOrder<T>(this.startNode);
    }    
    
	/** Reset the iterator.
	 */
	public void reset() {
		this.nextNode=this.startNode;
		this.currentNode=this.startNode;
	}
	

    private void walkMostLeftDownwards( ) {
    	if ( this.currentNode.hasChildren() ) {
    		//System.out.println("store index of node: " +  this.currentNode.toString()); //debug
    		this.childIndexes.push(this.currentNodeIndex);
    		this.currentNode=this.currentNode.getChildAt(0);
    		this.currentNodeIndex=0;
    		walkMostLeftDownwards();
    	} else {
         	//System.out.println("down : add current node to cache: " + this.currentNode.toString()); //debug
    	    this.nodeCache.add(this.currentNode); // add at end queue
    	}
    }
    
    private void  walkToNextSibling() {		
    	if ( this.currentNode == this.startNode ) {  
    		// we are back at root, so we are finished walking
    		this.currentNode=null; 
    		return;
    	} 
    	if ( this.currentNodeIndex + 1 < this.currentNode.getParent().getNumberOfChildren() ) {
    		//System.out.println("parent: " +  this.currentNode.getParent().toString()); //debug
    		//System.out.println("parent: " +  this.currentNode.getParent().getNumberOfChildren()); //debug
    		//System.out.println("child index: " + this.currentNodeIndex); //debug
    		// get right sibling
    		this.currentNodeIndex++;
    		this.currentNode=this.currentNode.getParent().getChildAt(this.currentNodeIndex);
    		//System.out.println("sibbling: " +  this.currentNode.toString()); //debug
    		//System.out.println("sibbling index: " + this.currentNodeIndex); //debug
    	} else {  
    		// go one node upwards and try walkToNextSibling again 
    		this.currentNode=this.currentNode.getParent();
    		this.currentNodeIndex=this.childIndexes.pop();
    		//System.out.println("upwards : add current node to cache: " + this.currentNode.toString()); //debug
    		this.nodeCache.add(this.currentNode); // add at end queue 		    		
  		    walkToNextSibling();
    	}    	
    	
    }
    
    private void walkToNextRightDownwardsSibling() {
       	// walk from parent downwards to first child  repeatedly
    	walkMostLeftDownwards();    	
    	
    	// try to find sibbling or else parents sibling or parent-parents sibling etc..
    	walkToNextSibling(); 
    }
    
    /* gets new value for this.nextNode
     * Note: if no next node node is found it return null 
     */
    GenericTreeNode<T> getNextNode() {   	
    	// find new this.nextNode node        	
    	if ( this.nodeCache.isEmpty() && this.currentNode != null   ) {
    		// nodeCache is empty so we must walk through to collect nodes in cache
    		// note: this.currentNode == null means we are finished walking
    		walkToNextRightDownwardsSibling(); // puts passing nodes at end of cache queue
        }
       
    	if ( this.nodeCache.isEmpty() ) { // if true: this.currentNode should be null
    		// no new nodes found by walking -> at end of walk through tree
    		return null;
    	} else {
          	return this.nodeCache.remove(); // remove from begin queue and return node    	    		
    	}
    	
    }
    
	/** Fetches the next node from the tree,
	 *  Note: this function is only called when next node exists!
	 */
	public GenericTreeNode<T> fetchNext() {
    	// when we are at root initial, we have to find the nextNode first
    	if ( this.currentNode == this.startNode ) {          		
    		this.nextNode=getNextNode();
    	}
    	
    	// this.nextNode becomes node to return
    	GenericTreeNode<T> node=this.nextNode;  
    	
        // prefetch next node for next call to this method
    	// getNextNode() will return null if whole tree is reached
    	// note: if this.nextNode is set to null then hasNext() will on next call return false on iteration will stop!
    	this.nextNode=getNextNode();   
    	
    	return node;  
	}
	

	
        
}    
