package generictree;


public abstract class GenericTreeIteratorAbstract<T> {

	/** The data to be iterated over. */
	protected GenericTreeNode<T> startNode;
    protected GenericTreeNode<T> nextNode;

	
    /** Construct an GenericTreeIterator object.
     * @param startNode The root Node of the tree to be iterated over.
     */  
    public GenericTreeIteratorAbstract(GenericTreeNode<T> startNode) {}
	

	/** Reset the iterator.
	 */
	public  void reset(){};
	

	/** Set the root Node to the given startNode, and reset the iterator.
	 * @param startNode The root Node of the tree to be iterated over.
	 * Note: by creating a GenericTreeIterator with a root node set
	 *       to some node in a bigger tree, we effectively have the
	 *       iterator only iterating through the subtree of the bigger
	 *       tree starting at the startNode! 
	 */
	public void setStartNode(GenericTreeNode<T> startNode) {
	    this.startNode = startNode;
	    this.reset();
	}
	
	public GenericTreeNode<T> getStartNode() {
	    return this.startNode;
	}	

	/** Fetches the next node from the tree,
	 *  Note: this function is only called when next node exists!
	 *        That is : fetchNext() is only called when hasNext() returns true!
	 */
	abstract public GenericTreeNode<T> fetchNext() ;
	
	
	/** 
	 * Tell if there are any more nodes in tree to traversal.
	 * @return true if not at the end, i.e., if next( ) will succeed.
	 * @return false if next( ) will throw an exception.
	 */
	public boolean hasNext() {
		return   ( this.nextNode != null )  ;
	}	
	
	/** Returns the next node from the tree */
	public GenericTreeNode<T> next() {
		if (hasNext()) {
      	     return fetchNext();	                 	
	    }
	    throw new IndexOutOfBoundsException("no nodes in tree to traversal anymore");	    
	}	

	/** Remove the object that next( ) just returned.
	 * An Iterator is not required to support this interface,
	 * and we certainly don't!
	 */
	public void remove() {
	    throw new UnsupportedOperationException(
	        "This demo does not implement the remove method");
	}

}