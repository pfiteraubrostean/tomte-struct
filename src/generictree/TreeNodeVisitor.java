package generictree;

import generictree.GenericTreeNode;

public interface TreeNodeVisitor<T> {
	public void  visit(GenericTreeNode<T> node );
}
