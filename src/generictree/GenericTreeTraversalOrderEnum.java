
package generictree;

public enum GenericTreeTraversalOrderEnum {
    PRE_ORDER,   // old
    POST_ORDER,  // old
    BREADTH_FIRST,
    DEPTH_FIRST_PRE_ORDER,
    DEPTH_FIRST_POST_ORDER 
}
