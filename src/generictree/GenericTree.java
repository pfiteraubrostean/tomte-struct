
package generictree;

import generictree.GenericTreeIteratorDepthFirstPreOrder;
import generictree.GenericTreeNode;

import java.util.*;
import java.lang.Iterable;

public class GenericTree<T> implements Iterable<GenericTreeNode<T>> {

    private GenericTreeNode<T> root;
    //private Iterator<GenericTreeNode<T>> iterator;
    private GenericTreeIterator<GenericTreeNode<T>> iterator;
    
    
    public GenericTree() {
        super();
    }
    
    public GenericTree<T> getShallowCopy() {
    	GenericTree<T> shallowCopy= new GenericTree<T>();
    	shallowCopy.setRoot(this.getRoot());
    	return shallowCopy;
    }

    public GenericTreeNode<T> getRoot() {
        return this.root;
    }
    
    public void setRoot(GenericTreeNode<T> root) {
        this.root = root;
        this.iterator=new GenericTreeIteratorDepthFirstPreOrder<T>(root);
    }
  
    public boolean isEmpty() {
        return (root == null);
    }
    
    // add iterator to GenericTree 
    
    public void setIterator( GenericTreeIterator<GenericTreeNode<T>> iteratorIn ) {      
       this.iterator=iteratorIn; 
    } 
     
     public GenericTreeIterator<GenericTreeNode<T>> iterator() {
     	return this.iterator.newInstance();
     } 
     

     
     public String toStringNew() {
         /*
         GenericTreeIteratorDepthFirstPreOrder<String> it = new GenericTreeIteratorDepthFirstPreOrder<String>(root);
         int i = 0;
         while (it.hasNext( )) {
             Object o = it.next( );
             System.out.println("Element " + i++ + " = " + o);    	   
         }
         */
     	ArrayList<String> str= new ArrayList<String>();
         //int i = 0;
         for(GenericTreeNode<T> node : this) {     	   
         	str.add(node.toString());        	
         }
         return str.toString();

       /*  
         str="";
         for(GenericTreeNode<T> node : this) {     	   
         	str= str + node.toString();        	
         }
         return str;
       */  
     }    
            
     
     
    
    public int getNumberOfNodes() {
        int numberOfNodes = 0;

        if(root != null) {
            numberOfNodes = auxiliaryGetNumberOfNodes(root) + 1; //1 for the root!
        }

        return numberOfNodes;
    }

    private int auxiliaryGetNumberOfNodes(GenericTreeNode<T> node) {
        int numberOfNodes = node.getNumberOfChildren();

        for(GenericTreeNode<T> child : node.getChildren()) {
            numberOfNodes += auxiliaryGetNumberOfNodes(child);
        }

        return numberOfNodes;
    }    
    
    public int getNumberOfEndNodes() {
        int numberOfEndNodes = 0;

        if(root != null) {
        	numberOfEndNodes = auxiliaryGetNumberOfEndNodes(root); 
        }

        return numberOfEndNodes;
    }
    
    
  

    private int auxiliaryGetNumberOfEndNodes(GenericTreeNode<T> node) {
    	int numberOfEndNodes = 0;
    	if (node.getNumberOfChildren() == 0 ) return 1;

        for(GenericTreeNode<T> child : node.getChildren()) {
        	numberOfEndNodes += auxiliaryGetNumberOfEndNodes(child);
        }

        return numberOfEndNodes;
    }      
    
    
    public Map <Integer,Integer> depthIndex() {
    	int depth=0;
        Map <Integer,Integer> depth2numNodes= new HashMap <Integer,Integer> ();        
        if(root != null) {
        	depth2numNodes.put(depth, 1);
        	depth2numNodes=auxiliaryDepthIndex(root,depth2numNodes,depth);
        }

        return depth2numNodes;
    }

    private Map <Integer,Integer> auxiliaryDepthIndex(GenericTreeNode<T> node,Map <Integer,Integer> depth2numNodes,int depth) {    	
    	

    	// fetch number of children; if that is 0, then nothing to do and return
    	int numChildren=node.getNumberOfChildren();
    	if ( numChildren == 0 ) return depth2numNodes;
    	
    	
    	// Let's add the number of nodes which are child of current node to the depth2numNodes map.
    	// So we have to increase depth with 1 to get the depth of the child nodes
    	depth++;     	
    	// Fetch the number of nodes already found at this depth
    	int numNodes;
    	if ( depth2numNodes.containsKey(depth) ) {
    		numNodes=depth2numNodes.get(depth);
    	} else {
    		numNodes=0;
    	}
    	// Set numNodes in array with value : already found nodes + num of children of current node
	    depth2numNodes.put(depth,numNodes+numChildren);
	    
	    // repeat this procedure for the each of the child nodes
        for(GenericTreeNode<T> child : node.getChildren()) {
        	depth2numNodes = auxiliaryDepthIndex(child,depth2numNodes,depth);
        }

        return depth2numNodes;
    }     
    
    
    public List<GenericTreeNode<T>> getNodeTrace(GenericTreeNode<T> currentNode) {
        ArrayList<GenericTreeNode<T>> returnList = null;

               
        if(root != null) {
        	returnList=new ArrayList<GenericTreeNode<T>> ();
			this.setIterator(new GenericTreeIteratorUpwards<T>(currentNode));      
			for(GenericTreeNode<T> node : this) {
				returnList.add(node);
			}
			Collections.reverse(returnList);        	
        }

        return returnList;
    }
    
    
//-------------------------------------------------------------------------------------------------    
/// think below is obsolete -> use iterator loop instead!
//-------------------------------------------------------------------------------------------------
    


//TODO: find should use set traversal method!    
    
    public boolean exists(T dataToFind) {
        return (find(dataToFind) != null);
    }

    public GenericTreeNode<T> find(T dataToFind) {
        GenericTreeNode<T> returnNode = null;

        if(root != null) {
            returnNode = auxiliaryFind(root, dataToFind);
        }

        return returnNode;
    }

    private GenericTreeNode<T> auxiliaryFind(GenericTreeNode<T> currentNode, T dataToFind) {
        GenericTreeNode<T> returnNode = null;
        int i = 0;

        if (currentNode.getData().equals(dataToFind)) {
            returnNode = currentNode;
        }

        else if(currentNode.hasChildren()) {
            i = 0;
            while(returnNode == null && i < currentNode.getNumberOfChildren()) {
                returnNode = auxiliaryFind(currentNode.getChildAt(i), dataToFind);
                i++;
            }
        }

        return returnNode;
    }



    
  
    
    
    
// map tree to a flat ArrayList (fixed order of insertion) or a LinkedHashMap (preserves iteration order : This linked list defines the iteration ordering, which is normally the order in which keys were inserted into the map (insertion-order).!)     
    
    
    public List<GenericTreeNode<T>> build(GenericTreeTraversalOrderEnum traversalOrder) {
        List<GenericTreeNode<T>> returnList = null;

        if(root != null) {
            returnList = build(root, traversalOrder);
        }

        return returnList;
    }

    public List<GenericTreeNode<T>> build(GenericTreeNode<T> node, GenericTreeTraversalOrderEnum traversalOrder) {
        List<GenericTreeNode<T>> traversalResult = new ArrayList<GenericTreeNode<T>>();

        if(traversalOrder == GenericTreeTraversalOrderEnum.PRE_ORDER) {
            buildPreOrder(node, traversalResult);
        }

        else if(traversalOrder == GenericTreeTraversalOrderEnum.POST_ORDER) {
            buildPostOrder(node, traversalResult);
        }

        return traversalResult;
    }

    private void buildPreOrder(GenericTreeNode<T> node, List<GenericTreeNode<T>> traversalResult) {
        traversalResult.add(node);

        for(GenericTreeNode<T> child : node.getChildren()) {
            buildPreOrder(child, traversalResult);
        }
    }

    private void buildPostOrder(GenericTreeNode<T> node, List<GenericTreeNode<T>> traversalResult) {
        for(GenericTreeNode<T> child : node.getChildren()) {
            buildPostOrder(child, traversalResult);
        }

        traversalResult.add(node);
    }

    public Map<GenericTreeNode<T>, Integer> buildWithDepth(GenericTreeTraversalOrderEnum traversalOrder) {
        Map<GenericTreeNode<T>, Integer> returnMap = null;

        if(root != null) {
            returnMap = buildWithDepth(root, traversalOrder);
        }

        return returnMap;
    }

    public Map<GenericTreeNode<T>, Integer> buildWithDepth(GenericTreeNode<T> node, GenericTreeTraversalOrderEnum traversalOrder) {
        Map<GenericTreeNode<T>, Integer> traversalResult = new LinkedHashMap<GenericTreeNode<T>, Integer>();

        if(traversalOrder == GenericTreeTraversalOrderEnum.PRE_ORDER) {
            buildPreOrderWithDepth(node, traversalResult, 0);
        }

        else if(traversalOrder == GenericTreeTraversalOrderEnum.POST_ORDER) {
            buildPostOrderWithDepth(node, traversalResult, 0);
        }

        return traversalResult;
    }

    private void buildPreOrderWithDepth(GenericTreeNode<T> node, Map<GenericTreeNode<T>, Integer> traversalResult, int depth) {
        traversalResult.put(node, depth);

        for(GenericTreeNode<T> child : node.getChildren()) {
            buildPreOrderWithDepth(child, traversalResult, depth + 1);
        }
    }

    private void buildPostOrderWithDepth(GenericTreeNode<T> node, Map<GenericTreeNode<T>, Integer> traversalResult, int depth) {
        for(GenericTreeNode<T> child : node.getChildren()) {
            buildPostOrderWithDepth(child, traversalResult, depth + 1);
        }

        traversalResult.put(node, depth);
    }

    public String toString() {
        /*
        We're going to assume a pre-order traversal by default
         */

        String stringRepresentation = "";

        if(root != null) {
            stringRepresentation = build(GenericTreeTraversalOrderEnum.PRE_ORDER).toString();

        }

        return stringRepresentation;
    }

    public String toStringWithDepth() {
        /*
        We're going to assume a pre-order traversal by default
         */

        String stringRepresentation = "";

        if(root != null) {
            stringRepresentation = buildWithDepth(GenericTreeTraversalOrderEnum.PRE_ORDER).toString();
        }

        return stringRepresentation;
    }
}
