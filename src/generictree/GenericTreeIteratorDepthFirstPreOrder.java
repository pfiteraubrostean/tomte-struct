package generictree;

import java.util.ListIterator;
import java.util.Stack;

public class GenericTreeIteratorDepthFirstPreOrder<T> extends GenericTreeIteratorAbstract<T> implements GenericTreeIterator<GenericTreeNode<T>> {

    protected Stack<GenericTreeNode<T>> visiting = new Stack<GenericTreeNode<T>>(); 
    
  
    //protected int currentDepth;
    
    /** Construct an GenericTreeIteratorDepthFirstPreOrder object.
     * @param startNode The node of the tree where to start the iteration.
     */
    public GenericTreeIteratorDepthFirstPreOrder(GenericTreeNode<T> startNode) {        
    	super(startNode);
    	setStartNode(startNode);
    }
    
   // public GenericTreeIteratorDepthFirstPreOrder() {
   // }
    public GenericTreeIteratorDepthFirstPreOrder<T> newInstance() {
    	return new GenericTreeIteratorDepthFirstPreOrder<T>(this.startNode);
    }     
    
	/** Reset the iterator.
	 */
	public void reset() {
		this.nextNode=this.startNode;
		visiting = new Stack<GenericTreeNode<T>>();
		visiting.push(this.startNode);
	}
	
	/** Fetches the next node from the tree,
	 *  Note: this function is only called when next node exists!
	 */	
	public GenericTreeNode<T> fetchNext(){
    	GenericTreeNode<T> node=visiting.pop();
    	
    	// add children of node on visiting stack
    	// note: children of node are in a ordered list and are added to stack in same order
    	//       For the right left to right walking order, we have to add the children in reversed order to the stack
   /* 	
    	for(GenericTreeNode<T> child : node.getChildren() ) {
        	visiting.push(child);
        }  
   */
    	ListIterator<GenericTreeNode<T>> it=node.getChildren().listIterator(node.getNumberOfChildren());
        while (it.hasPrevious( )) {
        	GenericTreeNode<T> child = it.previous( );
        	visiting.push(child);
        }        	
    	
    	// If nothing is pushed and visiting stack is empty we haved handled all tree nodes
    	if (visiting.empty()) { 
    	    this.nextNode = null; // meaning finished traversing tree 
    	}         	      	
    	return node;   		
    }
	

}    