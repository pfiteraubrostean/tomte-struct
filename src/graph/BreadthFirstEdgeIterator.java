package graph;

import java.util.Deque;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.Set;

public class BreadthFirstEdgeIterator<N,E extends Edge<N>> {
	public Graph<N,E> graph;
	Iterator<E> edgeIterator;
	
	public BreadthFirstEdgeIterator(Graph<N,E> graph, N n) {
		LinkedHashSet<E> sortedEdges = getEdgesFromNode (graph, n);
		this.edgeIterator = sortedEdges.iterator();
	}
	
	public BreadthFirstEdgeIterator(Graph<N,E> graph) {
		this(graph, graph.getRoot());
	}
	
	public boolean hasNext() {
		return this.edgeIterator.hasNext();
	}
	
	public E next() {
		return this.edgeIterator.next();
	}
	

	private LinkedHashSet<E> getEdgesFromNode(Graph<N, E> graph, N n) {
		LinkedHashSet<E> allEdges = new LinkedHashSet<E>();
		Deque<N> nodesToVisit = new LinkedList<N>();
		nodesToVisit.add(n);
		while (!nodesToVisit.isEmpty()) {
			N visitedNode = nodesToVisit.removeFirst();
			Set<E> edges = graph._getConnectedEdges(visitedNode);
			allEdges.addAll(edges);
			Set<N> nextNodes = graph.getConnectedNodes(visitedNode);
			nodesToVisit.addAll(nextNodes);
		}
		
		return allEdges;
	}
}
