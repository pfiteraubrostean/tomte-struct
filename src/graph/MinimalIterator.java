package graph;

public interface MinimalIterator<T> {
	public boolean hasNext();
	public T next();
}
