package graph;

import java.util.Deque;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.Set;

public class BreadthFirstNodeIterator<N,E extends Edge<N>> implements MinimalIterator<N>{
	
	private Iterator<N> nodeIterator;

	public BreadthFirstNodeIterator(Graph<N,E> graph, N n) {
		LinkedHashSet<N> nodes = getNodesFromNodeInBFO(graph, n);
		nodeIterator = nodes.iterator();
	}
	
	public BreadthFirstNodeIterator(Graph<N,E> graph) {
		this(graph, graph.getRoot());
	}
	
	

	public boolean hasNext() {
		return nodeIterator.hasNext();
	}

	@Override
	public N next() {
		return nodeIterator.next();
	}
	
	private LinkedHashSet<N> getNodesFromNodeInBFO(Graph<N, E> graph, N n) {
		LinkedHashSet<N> nodesVisited = new LinkedHashSet<N>();
		Deque<N> nodesToVisit = new LinkedList<N>();
		nodesToVisit.add(n);
		while (!nodesToVisit.isEmpty()) {
			N visitedNode = nodesToVisit.removeFirst();
			nodesVisited.add(visitedNode);
			Set<N> nextNodes = graph.getConnectedNodes(visitedNode);
			nodesToVisit.addAll(nextNodes);
		}
		
		return nodesVisited;
	}

}
