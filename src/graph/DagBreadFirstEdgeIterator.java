package graph;

import java.util.HashSet;

public class DagBreadFirstEdgeIterator<N,E extends Edge<N>> implements MinimalIterator<E>{

	private BreadthFirstEdgeIterator<N,E> edgeIterator;
	private HashSet<N> visitedNodes;
	private E nextEdge=null;
	
	public DagBreadFirstEdgeIterator(Graph<N,E> graph, N n) {
		this.edgeIterator=new BreadthFirstEdgeIterator<N,E>(graph,n);
		this.visitedNodes=new HashSet<N>();
	}
	
	public boolean hasNext() {
		if ( nextEdge!= null) {
			// edge already found
			return true;
		}
		// find an edge which points to an endNode not yet visited
        if ( ! this.edgeIterator.hasNext()  ) 
        	return false;				
		E edge = this.edgeIterator.next();
		while ( visitedNodes.contains(edge.endNode)  ) {
	         if ( ! this.edgeIterator.hasNext()  ) 
	        	return false;				
			 edge = this.edgeIterator.next();		 
		}
		// we found an edge which points to an endNode not yet visited
		nextEdge=edge;
		visitedNodes.add(nextEdge.endNode);
		return true;
	}
	
	public E next() {
        if ( ! this.hasNext()  ) 
        	return null;
        E returnEdge = this.nextEdge;
        this.nextEdge=null;
        return returnEdge;
	}	
}
