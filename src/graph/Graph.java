package graph;

import java.util.ArrayList;
import java.util.BitSet;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;
import java.util.function.Consumer;
import java.util.function.Predicate;

public class Graph <N,E extends Edge<N>>{
	private LinkedHashMap<N, Set<E>> adjacencyList;
	private EdgeFactory<N,E> edgeFactory;
	
	
	public Graph(EdgeFactory<N,E> edgeFactory) {
		super();
		this.adjacencyList = new LinkedHashMap<N, Set<E>>();
		this.edgeFactory = edgeFactory;
	}

	public Graph(LinkedHashMap<N, Set<E>> adjacencyList, EdgeFactory<N,E> edgeFactory) {
		super();
		this.adjacencyList = adjacencyList;
		this.edgeFactory = edgeFactory;
	}
	
	/**
	 * Returns a deep copy of the graph.
	 */
	public Graph <N, E> clone() {
		LinkedHashMap<N, Set<E>> adjacencyList = new LinkedHashMap<N, Set<E>>();
		Iterator<Entry<N, Set<E>>> iterator = this.adjacencyList.entrySet().iterator();
		while (iterator.hasNext()) {
			Entry<N, Set<E>> next = iterator.next();
			adjacencyList.put(next.getKey(), new LinkedHashSet<E>(next.getValue()));
		}
		return new Graph<N,E> (adjacencyList, this.edgeFactory);
	}
	
	/**
	 * Adds node to the graph with no edges connected.
	 * @return true if node added, false if node already exists
	 */
	public boolean addNode(N node) {
		boolean success = false;
		if (!containsNode(node)) {
			adjacencyList.put(node, new LinkedHashSet<E>());
			success = true;
		}
		return success;
	}
	
	/**
	 * Removes a node and all its associated edges.
	 * @return true if node removed, false if node wasn't found in graph.
	 */
	public boolean removeNode(N node) {
		boolean success = false;
		if (containsNode(node)) {
			adjacencyList.remove(node);
			for (Set<E> connectedEdges : this.adjacencyList.values()) {
				connectedEdges.removeIf(new Predicate<E>() {
					public boolean test(E t) {
						return t.endNode.equals(node);
					}});
			}
			success = true;
		}
		return success;
	}
	
	/**
	 * Adds an edge using the edge factory. No edge is added if one of the nodes is not 
	 * in the graph or if the {@code edgeFactory} cannot build this edge.
	 * 
	 * @return true if an edge between nodes was successfully added  <br/> false otherwise
	 */
	public boolean addEdge(N fromNode, N toNode) {
		boolean success = false;
		if (containsNode(fromNode) && containsNode(toNode)) {
			E newEdge = edgeFactory.buildEdge(fromNode, toNode);
			if (newEdge != null) {
				Set<E> fromNodeEdges = adjacencyList.get(fromNode);
				fromNodeEdges.add(newEdge);
				success = true;
			}
		}
		return success;
	}
	
	/**
	 * Checks if node is contained in graph.
	 * @return true if node is in graph <br/> false node not in graph  
	 */
	public boolean containsNode(N node) {
		return _getNodes().contains(node);
	}

	/**
	 * Returns a fresh linked set of the graph's nodes.
	 */
	public LinkedHashSet<N> getNodes() {
		return new LinkedHashSet<N>(adjacencyList.keySet());
	}
	
	/**
	 * Returns the internal ordered set of the graph's nodes.
	 */
	protected Set<N> _getNodes() {
		return  adjacencyList.keySet();
	}
	
	
	protected LinkedHashSet<N> getConnectedNodes(N node) {
		LinkedHashSet<N> allNodes = new LinkedHashSet<N>();
		if (containsNode(node)) {
			Set<E> fromNodeEdges = adjacencyList.get(node);
			for (E edge : fromNodeEdges) {
				allNodes.add(edge.endNode);
			}
		}
		return allNodes;
	}
	
	/**
	 * Returns the internal set of the edges connected to the node.
	 */
	protected Set<E> _getConnectedEdges(N node) {
		return adjacencyList.get(node);
	}
	
	/**
	 * Returns the root or the graph or null if the graph is empty. 
	 * The root is the first node added to the graph.
	 */
	public N getRoot() {
		return size() == 0 ? null : _getNodes().iterator().next();
	}
	
	/**
	 * Gets all other nodes in graph then the ones given
	 * @param nodes
	 * @return complementNodes
	 */
	protected Set<N> getComplementNodes(Set<N> nodes){
		Set<N> complementNodes = getNodes();
		complementNodes.removeAll(nodes);
        return  complementNodes; 
	}
	

	public Split<N,E> split(BitSet encodedSplit) {
		Set<N> nodes = getNodesFromCode(encodedSplit);
		Set<N> complementNodes = getNodes();
		complementNodes.removeAll(nodes);
		return new Split<N,E> (subgraph(nodes),subgraph(complementNodes));
	}
	
	public Split<N,E> split(List<Integer> encodedSplit) {
		LinkedHashSet<N> nodes = getNodesFromCode(encodedSplit);
		LinkedHashSet<N> complementNodes = getNodes();
		complementNodes.removeAll(nodes);
		return new Split<N,E>(subgraph(nodes),subgraph(complementNodes));
	}
	
	private LinkedHashSet<N> getNodesFromCode(List<Integer> encodedSplit) {
		List<N> nodes = new ArrayList<N>(_getNodes());
		LinkedHashSet<N> splitNodes = new LinkedHashSet<N>();
		nodes.forEach(new Consumer<N>() {
			private int index = 0;
			int min = encodedSplit.size()>= nodes.size()? nodes.size() : encodedSplit.size();
			public void accept(N t) {
				if (index < min &&  encodedSplit.get(index) == 1) {
					splitNodes.add(t);
				}
				index ++;
			}
		});
		return splitNodes;
	}
	
	private LinkedHashSet<N> getNodesFromCode(BitSet encodedSplit) {
		int min = encodedSplit.length() >= size()? size() : encodedSplit.length();
		List<N> nodes = new ArrayList<N>(_getNodes());
		LinkedHashSet<N> selectedNodes = new LinkedHashSet<N>();
		for(int nodeIndex = 0; nodeIndex < min; nodeIndex ++) {
			if (encodedSplit.get(nodeIndex)) {
				selectedNodes.add(nodes.get(nodeIndex));
			}
		}
		return selectedNodes;
	}

	/**
	 * Builds a subgraph containing the nodes given by the split.
	 */
	public Graph<N,E> subgraph( Set<N> split) {
		Set<N> nodesToRemove=getComplementNodes(split);
		Graph<N, E> clone = this.clone();
		for (N node : nodesToRemove) {
			clone.removeNode(node);
		}
		return clone;
	}
	
	/**
	 * Checks if from the root all other nodes can be reached.
	 */
	public boolean isConnected() {
		BreadthFirstNodeIterator<N,E> nodeIterator = new BreadthFirstNodeIterator<N,E>(this, getRoot());
		Set<N> nodesReachedFromRoot = new HashSet<N>();
		while(nodeIterator.hasNext()) {
			nodesReachedFromRoot.add(nodeIterator.next());
		}
		Set<N> nodesInGraph = _getNodes();
				
		return nodesReachedFromRoot.size() == nodesInGraph.size();
	}
	
	public int size() {
		return this.adjacencyList.size();
	}
	
	public String toString() {
		BreadthFirstNodeIterator<N,E> nodeIterator = new BreadthFirstNodeIterator<N,E>(this, getRoot());
		StringBuilder builder = new StringBuilder();
		while(nodeIterator.hasNext()) {
			N currentNode = nodeIterator.next();
			builder.append(currentNode).append(" { \n");
			for (E edge : this._getConnectedEdges(currentNode)) {
				builder.append("  ").append(edge.toString()).append("\n");
			}
			builder.append("}\n");
		}
		return builder.toString();
	}
}
