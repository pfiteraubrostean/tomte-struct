package graph.content;

import graph.Edge;

/**
 * An edge implementing processing over content. 
 */
public abstract class ProcessingEdge <C,N extends ContentNode<C>> extends Edge<N> implements ContentProcessor<C>{

	public ProcessingEdge(N startNode, N endNode) {
		super(startNode, endNode);
	}
	
	/**
	 * Processes content from the {@code startNode} and pushes it to the {@code endNode}. This makes an edge consistent.
	 */
	public void update() {
		C content = super.startNode.getContent();
		C contentForNode = processContent(content);
		super.endNode.setContent(contentForNode);
	}

	public abstract C processContent(C content);
}
