package graph.content.example;

import java.util.Arrays;

import graph.Split;

public class Test {
	public static void main(String args []) {
		SuffixClosedGraph graph = new SuffixClosedGraph();
		graph.addNode(new StringNode("a"));
		graph.addNode(new StringNode("ab"));
		graph.addNode(new StringNode("ac"));
		graph.addNode(new StringNode("abc"));
		graph.addNode(new StringNode("abcd"));
		
		System.out.println(graph);
		
		System.out.println(graph.isConnected());
		
		graph.addNode(new StringNode("b"));
		
		System.out.println(graph.isConnected());
		
		Split<StringNode, ConcatEdge> split = graph.split(Arrays.asList(1,1,1,1,1,0));
		System.out.println(split.first.isConnected());
		System.out.println(split.second.isConnected());
	}
}
