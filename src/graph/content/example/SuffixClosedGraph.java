package graph.content.example;

import graph.content.ContentDagGraph;

public class SuffixClosedGraph extends ContentDagGraph<String, StringNode, ConcatEdge> {

	public SuffixClosedGraph() {
		super(new ConcatEdgeFactory());
	}

}
