package graph.content.example;

import graph.content.ValueNode;

public class StringNode extends ValueNode<String>{

	public StringNode(String value) {
		super(value);
	}

	public String toString() {
		return super.getContent();
	}
}
