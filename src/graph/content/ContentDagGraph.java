package graph.content;

import java.util.function.Consumer;

import graph.DagBreadFirstEdgeIterator;
import graph.EdgeFactory;
import graph.Graph;

/**
 * A direct acyclic graph where each node encapsulates content
 * and each edge implements a processing over this content. The
 * content of the start node of an edge is processed, 
 * the resulting content is then pushed to the end node.
 */
public class ContentDagGraph<C, N extends ContentNode<C>, E extends ProcessingEdge<C,N>> extends Graph<N,E>{
	public ContentDagGraph(EdgeFactory<N,E> edgeFactory) {
		super(edgeFactory);
	}
	
	/**
	 * Adds a node to the graph, plus edges connecting all other nodes to this node.
	 * Edges are not added if the {@code edgeFactory} cannot make an edge connecting 
	 * the two nodes.
	 */
	public boolean addNode(final N node) {
		boolean success = super.addNode(node);
		if (success) {
			_getNodes().forEach(new Consumer<N>(){
				public void accept(N crtNode) {
					if (!node.equals(crtNode)) {
						addEdge(crtNode,node);
					}
				}});
		}
		
		return success;
	}
	
	/**
	 * Sets the content of the root node to {@code newContent} and starts a processing
	 * chain of all edges. 
	 */
	public void updateContent( C newContent) {
		N node = super.getRoot();
		node.setContent(newContent);
		DagBreadFirstEdgeIterator<N, E> iterator = new DagBreadFirstEdgeIterator<N,E>(this, node);
		while (iterator.hasNext()) {
			E edge = iterator.next();
			edge.update();
		}
	}
}
