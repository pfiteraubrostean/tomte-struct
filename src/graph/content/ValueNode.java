package graph.content;

/**
 * A node encapsulating a generic value.
 */
public class ValueNode<C> implements ContentNode<C>{
	private C value;

	public ValueNode(C value) {
		this.value = value;
	}
	
	@Override
	public void setContent(C content) {
		this.value = content;
	}

	@Override
	public C getContent() {
		return this.value;
	}

}
