package graph.content.relation;

import graph.content.ValueNode;

public class ParameterNode<C> extends ValueNode<C> {

	public final int actionIndex;
	public final int paramIndex;

	public ParameterNode(C value, int actionIndex, int paramIndex) {
		super(value);
		this.actionIndex = actionIndex;
		this.paramIndex = paramIndex;
	}

}
