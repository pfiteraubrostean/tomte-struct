package graph.content.relation;

import graph.content.ProcessingEdge;
import graph.content.ValueNode;
import relation.Relation;

public class RelationEdge<N extends ValueNode<Integer>> extends ProcessingEdge<Integer, N>{
	
	private Relation relation;

	public RelationEdge(N startNode, N endNode, Relation relation) {
		super(startNode, endNode);
		this.relation = relation;
	}

	public Integer processContent(Integer content) {
		return relation.map(content);
	}
}
