package graph.content.relation;

import graph.EdgeFactory;
import graph.content.ValueNode;
import relation.Relation;

public class RelationEdgeFactory<N extends ValueNode<Integer>> implements EdgeFactory<N, RelationEdge<N>> {

	public RelationEdgeFactory() {
	}
	
	/**
	 * Builds an relation edge. An edge can only be built between nodes whose
	 * values are related.
	 */
	public RelationEdge<N> buildEdge(N fromNode, N toNode) {
		Integer fromValue = fromNode.getContent();
		Integer toValue = toNode.getContent();
		if (Relation.areRelated(fromValue, toValue)) {
			return new RelationEdge<N>(fromNode, toNode, Relation.getRelation(fromValue, toValue));
		}
		return null;
	}
	
}
