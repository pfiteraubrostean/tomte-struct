package graph.content.relation;

import graph.content.ContentDagGraph;

/*
 * Relation graph over Integer parameter nodes.
 */
public class RelationGraph extends ContentDagGraph<Integer, ParameterNode<Integer>, RelationEdge<ParameterNode<Integer>>>{

	public RelationGraph() {
		super(new RelationParameterEdgeFactory());
	}

}
