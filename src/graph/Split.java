package graph;

public class Split<N,E extends Edge<N>> {
	public Split(Graph<N, E> first, Graph<N, E> second) {
		super();
		this.first = first;
		this.second = second;
	}
	public final Graph<N,E> first;
	public final Graph<N,E> second;
}
