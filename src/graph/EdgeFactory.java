package graph;

public  interface EdgeFactory <N,E> {
	/**
	 * Builds an edge between two nodes. In case the edge cannot be built, returns null.
	 * @param fromNode
	 * @param toNode
	 * @return
	 */
	public E buildEdge(N fromNode, N toNode);
}
